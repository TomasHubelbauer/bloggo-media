# Media

- [ ] Consolidate the 3D, Video etc. posts all under this one creativity umbrella
- [ ] Check out [YouTube Desktop streaming](https://techcrunch.com/2018/03/20/youtube-rolls-out-a-new-feature-that-lets-you-go-live-from-the-desktop-without-an-encoder)

## Video

- [GitLab repository](https://gitlab.com/TomasHubelbauer/bloggo-video)
- [Bloggo post](http://hubelbauer.net/post/bloggo-video)

## Image

- [Inpainting - erasing objects from photos (C#)](https://github.com/zavolokas/Inpainting)

### Photopea

https://www.photopea.com/

### FastPhotoStyle - Photo style transfer

https://github.com/NVIDIA/FastPhotoStyle

## Color

[Color: From Hexcodes to Eyeballs](http://jamie-wong.com/post/color/) by Jamie Wong
